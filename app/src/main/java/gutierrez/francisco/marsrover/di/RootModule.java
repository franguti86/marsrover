package gutierrez.francisco.marsrover.di;

import android.content.Context;
import android.view.LayoutInflater;

import dagger.Module;
import dagger.Provides;
import gutierrez.francisco.marsrover.MarsRoverApp;
import gutierrez.francisco.marsrover.domain.DomainModule;

@Module(
    includes = {
        DomainModule.class
    },
    injects = {
        MarsRoverApp.class
    },
    library = true) public class RootModule {

  private final Context context;

  public RootModule(Context context) {
    this.context = context;
  }

  @Provides Context provideApplicationContext() {
    return context;
  }

  @Provides LayoutInflater provideLayoutInflater() {
    return LayoutInflater.from(context);
  }

}
