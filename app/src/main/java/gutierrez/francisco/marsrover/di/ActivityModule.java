package gutierrez.francisco.marsrover.di;

import android.content.Context;
import android.support.v4.app.FragmentActivity;

import dagger.Module;
import dagger.Provides;

@Module(library = true)
public class ActivityModule {

  private final FragmentActivity activity;

  public ActivityModule(FragmentActivity activity) {
    this.activity = activity;
  }

  @ActivityContext @Provides Context provideActivityContext() {
    return activity;
  }

}
