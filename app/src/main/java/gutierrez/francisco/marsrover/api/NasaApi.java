package gutierrez.francisco.marsrover.api;

import gutierrez.francisco.marsrover.api.model.RetrievePhotoResponse;
import retrofit.Call;
import retrofit.http.GET;

public interface NasaApi {

  @GET("/mars-photos/api/v1/rovers/curiosity/photos?sol=1000&api_key=DEMO_KEY")
  Call<RetrievePhotoResponse> retrievePhotos();

}
