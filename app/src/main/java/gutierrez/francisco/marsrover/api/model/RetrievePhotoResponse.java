package gutierrez.francisco.marsrover.api.model;

import com.google.gson.annotations.SerializedName;

import gutierrez.francisco.marsrover.entities.Photo;

public class RetrievePhotoResponse {

    @SerializedName("photos") public Photo[] photos;

}
