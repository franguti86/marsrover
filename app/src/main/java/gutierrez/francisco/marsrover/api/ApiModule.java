package gutierrez.francisco.marsrover.api;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import gutierrez.francisco.marsrover.Constants;
import gutierrez.francisco.marsrover.di.Named;
import gutierrez.francisco.marsrover.utils.CurlLoggingInterceptor;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

@Module(library = true)
public class ApiModule {

    @Provides
    @Named("nasa_endpoint")
    String provideNasaEndpoint() {
        return Constants.NASA_BASE_ENDPOINT;
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient() {
        OkHttpClient okClient = new OkHttpClient();
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        okClient.interceptors().add(httpLoggingInterceptor);
        okClient.interceptors().add(new CurlLoggingInterceptor());
        return okClient;
    }

    @Provides
    Retrofit provideRestAdapter(OkHttpClient okClient, @Named("nasa_endpoint") String endpoint) {
        return new Retrofit.Builder()
                .baseUrl(endpoint)
                .client(okClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Provides
    @Singleton
    NasaApi provideNasaRestAPI(Retrofit restAdapter) {
        return restAdapter.create(NasaApi.class);
    }

}
