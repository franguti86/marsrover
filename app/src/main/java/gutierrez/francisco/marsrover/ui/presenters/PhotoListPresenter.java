package gutierrez.francisco.marsrover.ui.presenters;

import javax.inject.Inject;

import gutierrez.francisco.marsrover.domain.interactors.RetrievePhotosInteractor;
import gutierrez.francisco.marsrover.entities.Photo;
import gutierrez.francisco.marsrover.ui.Navigator;

public class PhotoListPresenter implements RetrievePhotosInteractor.Callback {

    private final RetrievePhotosInteractor retrievePhotosInteractor;
    private final Navigator navigator;
    private PhotoListView view;

    @Inject
    public PhotoListPresenter(RetrievePhotosInteractor retrievePhotosInteractor, Navigator navigator) {
        this.retrievePhotosInteractor = retrievePhotosInteractor;
        this.navigator = navigator;
    }

    public void setView(PhotoListView view) {
        this.view = view;
    }

    public void start() {
        view.showProgressBar();
        retrievePhotosInteractor.retrievePhotos(this);
    }

    @Override
    public void onRetrievePhotosSuccess(Photo[] photos) {
        view.hideProgressBar();
        view.showPhotos(photos);
    }

    @Override
    public void onRetrievePhotosError() {
        view.hideProgressBar();
        view.showError();
    }

    public void performShowPhotoDetail(Photo photo) {
        navigator.openPhotoDetail(photo);
    }
}
