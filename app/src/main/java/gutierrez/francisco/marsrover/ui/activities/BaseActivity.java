package gutierrez.francisco.marsrover.ui.activities;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import dagger.ObjectGraph;
import gutierrez.francisco.marsrover.MarsRoverApp;
import gutierrez.francisco.marsrover.di.ActivityModule;

public abstract class BaseActivity extends AppCompatActivity {

  private ObjectGraph activityScopeGraph;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    injectDependencies();
    setContentView(getActivityLayout());
    injectViews();
  }

  @LayoutRes
  protected abstract int getActivityLayout();

  public void inject(Object object) {
    activityScopeGraph.inject(object);
  }

  protected abstract List<Object> getModules();

  private void injectDependencies() {
    MarsRoverApp app = (MarsRoverApp) getApplication();
    List<Object> activityScopeModules = getModules();
    if (activityScopeModules == null) activityScopeModules = new ArrayList<>(1);
    activityScopeModules.add(new ActivityModule(this));
    activityScopeGraph = app.plus(activityScopeModules);
    inject(this);
  }

  private void injectViews() {
    ButterKnife.bind(this);
  }
}
