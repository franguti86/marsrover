package gutierrez.francisco.marsrover.ui;

import dagger.Module;
import gutierrez.francisco.marsrover.ui.activities.PhotoDetailActivity;
import gutierrez.francisco.marsrover.ui.activities.PhotoListActivity;
import gutierrez.francisco.marsrover.ui.activities.SplashActivity;

@Module(
    injects = {
            SplashActivity.class, PhotoListActivity.class, PhotoDetailActivity.class
    },
    complete = false)
public class UIModule {

}
