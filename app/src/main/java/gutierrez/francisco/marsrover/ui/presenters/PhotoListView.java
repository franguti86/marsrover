package gutierrez.francisco.marsrover.ui.presenters;

import gutierrez.francisco.marsrover.entities.Photo;

public interface PhotoListView {

    void showPhotos(Photo[] photos);
    void showProgressBar();
    void hideProgressBar();
    void showError();
}
