package gutierrez.francisco.marsrover.ui.presenters;

import javax.inject.Inject;

import gutierrez.francisco.marsrover.entities.Photo;

public class PhotoDetailPresenter {

    private PhotoDetailView view;

    @Inject public PhotoDetailPresenter() { }

    public void setView(PhotoDetailView view) {
        this.view = view;
    }

    public void start(Photo photo) {
        view.showProgressBar();
        view.showPhoto(photo);
    }

    public void onPhotoLoaded() {
        view.hideProgressBar();
        view.showZoomTutorial();
    }

    public void onPhotoLoadedError() {
        view.hideProgressBar();
        view.showError();
    }
}
