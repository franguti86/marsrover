package gutierrez.francisco.marsrover.ui.activities;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import gutierrez.francisco.marsrover.R;
import gutierrez.francisco.marsrover.entities.Photo;
import gutierrez.francisco.marsrover.ui.UIModule;
import gutierrez.francisco.marsrover.ui.adapters.PhotoRecyclerViewAdapter;
import gutierrez.francisco.marsrover.ui.presenters.PhotoListPresenter;
import gutierrez.francisco.marsrover.ui.presenters.PhotoListView;
import gutierrez.francisco.marsrover.ui.views.ProgressBarView;

public class PhotoListActivity extends BaseActivity implements PhotoListView, PhotoRecyclerViewAdapter.PhotoRecyclerViewAdapterListener {

    @Inject
    PhotoListPresenter presenter;
    @Inject
    PhotoRecyclerViewAdapter photoRecyclerViewAdapter;

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.photo_recyclerview)
    RecyclerView photoRecyclerView;
    @Bind(R.id.progress_bar_view)
    ProgressBarView progressBarView;
    @Bind(R.id.container)
    ViewGroup container;

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_photo_list;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.setView(this);
        initialiseToolbar();
        initialiseRecyclerView();
        presenter.start();
    }

    private void initialiseToolbar() {
        setSupportActionBar(toolbar);
    }

    private void initialiseRecyclerView() {
        photoRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager =
                new LinearLayoutManager(getApplicationContext());
        photoRecyclerView.setLayoutManager(layoutManager);
        photoRecyclerView.setAdapter(photoRecyclerViewAdapter);
        photoRecyclerViewAdapter.setListener(this);
    }

    @Override
    protected List<Object> getModules() {
        List<Object> modules = new ArrayList<>();
        modules.add(new UIModule());
        return modules;
    }

    @Override
    public void showPhotos(Photo[] photos) {
        photoRecyclerViewAdapter.setPhotos(photos);
    }

    @Override
    public void showProgressBar() {
        progressBarView.setVisibility(View.VISIBLE);
        progressBarView.start();
    }

    @Override
    public void hideProgressBar() {
        progressBarView.stop();
        progressBarView.setVisibility(View.GONE);
    }

    @Override
    public void showError() {
        Snackbar.make(container, R.string.photo_list_retrieve_photos_error, Snackbar.LENGTH_LONG)
                .show();
    }

    @Override
    public void onPhotoSelected(Photo photo) {
        presenter.performShowPhotoDetail(photo);
    }
}
