package gutierrez.francisco.marsrover.ui.presenters;

import gutierrez.francisco.marsrover.entities.Photo;

public interface PhotoDetailView {

    void showPhoto(Photo photo);
    void showZoomTutorial();
    void showProgressBar();
    void hideProgressBar();
    void showError();
}
