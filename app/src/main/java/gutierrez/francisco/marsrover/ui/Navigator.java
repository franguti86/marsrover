package gutierrez.francisco.marsrover.ui;

import android.content.Context;
import android.content.Intent;

import javax.inject.Inject;

import gutierrez.francisco.marsrover.di.ActivityContext;
import gutierrez.francisco.marsrover.entities.Photo;
import gutierrez.francisco.marsrover.ui.activities.PhotoDetailActivity;
import gutierrez.francisco.marsrover.ui.activities.PhotoListActivity;

public class Navigator {

    private final Context activityContext;

    @Inject
    public Navigator(@ActivityContext Context activityContext) {
        this.activityContext = activityContext;
    }

    public void openPhotoDetail(Photo photo) {
        PhotoDetailActivity.start(activityContext, photo);
    }

    public void openPhotoList() {
        Intent intent = new Intent(activityContext, PhotoListActivity.class);
        activityContext.startActivity(intent);
    }
}
