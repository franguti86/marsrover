package gutierrez.francisco.marsrover.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import gutierrez.francisco.marsrover.R;
import gutierrez.francisco.marsrover.entities.Photo;
import gutierrez.francisco.marsrover.ui.UIModule;
import gutierrez.francisco.marsrover.ui.presenters.PhotoDetailPresenter;
import gutierrez.francisco.marsrover.ui.presenters.PhotoDetailView;
import gutierrez.francisco.marsrover.ui.views.ProgressBarView;
import uk.co.senab.photoview.PhotoViewAttacher;

public class PhotoDetailActivity extends BaseActivity implements PhotoDetailView {

    public static final String EXTRA_PHOTO = "EXTRA_PHOTO";

    @Inject
    PhotoDetailPresenter presenter;

    @Bind(R.id.container)
    ViewGroup container;
    @Bind(R.id.photo_imageview)
    ImageView photoImageView;
    @Bind(R.id.pinch_zoom_textview)
    TextView pinchZoomTextView;
    @Bind(R.id.progress_bar_view)
    ProgressBarView progressBarView;

    PhotoViewAttacher attacher;

    public static void start(Context activityContext, Photo photo) {
        Intent intent = new Intent(activityContext, PhotoDetailActivity.class);
        intent.putExtra(EXTRA_PHOTO, photo);
        activityContext.startActivity(intent);
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_photo_detail;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.setView(this);
        presenter.start(getPhoto());
    }

    @Override
    protected List<Object> getModules() {
        List<Object> modules = new ArrayList<>();
        modules.add(new UIModule());
        return modules;
    }

    private Photo getPhoto() {
        return getIntent().getExtras().getParcelable(EXTRA_PHOTO);
    }

    @Override
    public void showPhoto(Photo photo) {
        attacher = new PhotoViewAttacher(photoImageView);
        Picasso.with(this).load(photo.getImgSrc()).into(photoImageView, new Callback() {
            @Override
            public void onSuccess() {
                presenter.onPhotoLoaded();
                attacher.update();
            }

            @Override
            public void onError() {
                presenter.onPhotoLoadedError();
            }
        });
    }

    @Override
    public void showZoomTutorial() {
        pinchZoomTextView.setVisibility(View.VISIBLE);
        pinchZoomTextView.animate()
                .alpha(0)
                .setDuration(3000)
                .withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        pinchZoomTextView.setVisibility(View.GONE);
                    }
                })
                .start();
    }

    @Override
    public void showProgressBar() {
        progressBarView.setVisibility(View.VISIBLE);
        progressBarView.start();
    }

    @Override
    public void hideProgressBar() {
        progressBarView.stop();
        progressBarView.setVisibility(View.GONE);
    }

    @Override
    public void showError() {
        Snackbar.make(container, R.string.photo_detail_load_photo_error, Snackbar.LENGTH_LONG)
                .show();
    }
}
