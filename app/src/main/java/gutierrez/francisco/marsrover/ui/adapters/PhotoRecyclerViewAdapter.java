package gutierrez.francisco.marsrover.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import gutierrez.francisco.marsrover.R;
import gutierrez.francisco.marsrover.entities.Photo;
import gutierrez.francisco.marsrover.utils.Utils;

public class PhotoRecyclerViewAdapter extends RecyclerView.Adapter<PhotoRecyclerViewAdapter.PhotoViewHolder>
        implements View.OnClickListener {

    public interface PhotoRecyclerViewAdapterListener {
        void onPhotoSelected(Photo photo);
    }

    private final LayoutInflater layoutInflater;
    private Photo[] photos = new Photo[0];
    private PhotoRecyclerViewAdapterListener listener;

    @Inject
    public PhotoRecyclerViewAdapter(LayoutInflater layoutInflater) {
        this.layoutInflater = layoutInflater;
    }

    @Override
    public PhotoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.card_view_photo, parent, false);
        PhotoViewHolder holder = new PhotoViewHolder(view);
        view.setOnClickListener(this);
        return holder;
    }

    @Override
    public void onBindViewHolder(PhotoViewHolder holder, int position) {
        holder.bindData(photos[position], position);
    }

    @Override
    public int getItemCount() {
        return photos.length;
    }

    public void setPhotos(Photo[] photos) {
        this.photos = photos;
        notifyDataSetChanged();
    }

    public void setListener(PhotoRecyclerViewAdapterListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {
        Photo photo = (Photo) v.getTag();
        if (listener != null) listener.onPhotoSelected(photo);
    }

    static class PhotoViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.container)
        View containerView;
        @Bind(R.id.earth_date_textview)
        TextView earthDateTextView;
        @Bind(R.id.camera_full_name_textview)
        TextView cameraFullNameTextView;
        @Bind(R.id.photo_imageview)
        ImageView photoImageView;

        public PhotoViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindData(Photo photo, int position) {
            containerView.setTag(photo);
            earthDateTextView.setText(photo.getEarthDate());
            cameraFullNameTextView.setText(photo.getCamera().getFullName());
            Context context = photoImageView.getContext();
            Picasso.with(context)
                    .load(photo.getImgSrc())
                    .placeholder(R.drawable.ic_sketch_placeholder)
                    .resize((int) Utils.dpTopx(context, 100), (int) Utils.dpTopx(context, 100))
                    .into(photoImageView);
            int colorRes = position % 2 == 0 ? R.drawable.card_view_photo_even_selector
                    : R.drawable.card_view_photo_odd_selector;
            containerView.setBackgroundResource(colorRes);
        }

    }

}
