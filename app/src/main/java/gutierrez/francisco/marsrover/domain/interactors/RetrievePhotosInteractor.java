package gutierrez.francisco.marsrover.domain.interactors;

import gutierrez.francisco.marsrover.entities.Photo;

public interface RetrievePhotosInteractor {

    interface Callback {
        void onRetrievePhotosSuccess(Photo[] photos);
        void onRetrievePhotosError();
    }

    void retrievePhotos(Callback callback);

}
