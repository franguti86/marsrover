package gutierrez.francisco.marsrover.domain;

import dagger.Module;
import dagger.Provides;
import gutierrez.francisco.marsrover.api.ApiModule;
import gutierrez.francisco.marsrover.domain.interactors.RetrievePhotosInteractor;
import gutierrez.francisco.marsrover.domain.interactors.RetrievePhotosInteractorImpl;

@Module(
        includes = ApiModule.class,
        library = true,
        complete = false)
public class DomainModule {

    @Provides
    RetrievePhotosInteractor provideRetrievePhotosInteractor(
            RetrievePhotosInteractorImpl retrievePhotosInteractorImpl) {
        return retrievePhotosInteractorImpl;
    }

}
