package gutierrez.francisco.marsrover.domain.interactors;

import javax.inject.Inject;

import gutierrez.francisco.marsrover.api.NasaApi;
import gutierrez.francisco.marsrover.api.model.RetrievePhotoResponse;
import retrofit.Response;
import retrofit.Retrofit;

public class RetrievePhotosInteractorImpl implements RetrievePhotosInteractor {

    private final NasaApi nasaApi;

    @Inject public RetrievePhotosInteractorImpl(NasaApi nasaApi) {
        this.nasaApi = nasaApi;
    }

    @Override
    public void retrievePhotos(final Callback callback) {
        nasaApi.retrievePhotos().enqueue(new retrofit.Callback<RetrievePhotoResponse>() {
            @Override
            public void onResponse(Response<RetrievePhotoResponse> response, Retrofit retrofit) {
                if (response.code() == 200) {
                    RetrievePhotoResponse retrievePhotoResponse = response.body();
                    callback.onRetrievePhotosSuccess(retrievePhotoResponse.photos);
                } else {
                    callback.onRetrievePhotosError();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                callback.onRetrievePhotosError();
            }
        });
    }

}
