package gutierrez.francisco.marsrover.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Photo implements Parcelable {

    @SerializedName("earth_date") private String earthDate;
    @SerializedName("img_src") private String imgSrc;
    @SerializedName("camera") private Camera camera;

    public Photo() { }

    public Photo(String earthDate, String imgSrc, Camera camera) {
        this.earthDate = earthDate;
        this.imgSrc = imgSrc;
        this.camera = camera;
    }

    public String getEarthDate() {
        return earthDate;
    }

    public void setEarthDate(String earthDate) {
        this.earthDate = earthDate;
    }

    public String getImgSrc() {
        return imgSrc;
    }

    public void setImgSrc(String imgSrc) {
        this.imgSrc = imgSrc;
    }

    public Camera getCamera() {
        return camera;
    }

    public void setCamera(Camera camera) {
        this.camera = camera;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.earthDate);
        dest.writeString(this.imgSrc);
        dest.writeParcelable(this.camera, flags);
    }

    protected Photo(Parcel in) {
        this.earthDate = in.readString();
        this.imgSrc = in.readString();
        this.camera = in.readParcelable(Camera.class.getClassLoader());
    }

    public static final Parcelable.Creator<Photo> CREATOR = new Parcelable.Creator<Photo>() {
        public Photo createFromParcel(Parcel source) {
            return new Photo(source);
        }

        public Photo[] newArray(int size) {
            return new Photo[size];
        }
    };
}
