package gutierrez.francisco.marsrover.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Camera implements Parcelable {

    @SerializedName("full_name") private String fullName;

    public Camera() { }

    public Camera(String fullName) {
        this.fullName = fullName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.fullName);
    }

    protected Camera(Parcel in) {
        this.fullName = in.readString();
    }

    public static final Parcelable.Creator<Camera> CREATOR = new Parcelable.Creator<Camera>() {
        public Camera createFromParcel(Parcel source) {
            return new Camera(source);
        }

        public Camera[] newArray(int size) {
            return new Camera[size];
        }
    };
}
