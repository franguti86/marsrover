package gutierrez.francisco.marsrover.ui.presenters;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.inject.Inject;

import dagger.ObjectGraph;
import gutierrez.francisco.marsrover.entities.Photo;
import gutierrez.francisco.marsrover.ui.TestUIModule;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;

public class PhotoDetailPresenterTest {

    @Inject PhotoDetailPresenter presenter;

    @Mock PhotoDetailView mockView;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        ObjectGraph objectGraph = ObjectGraph.create(new TestUIModule());
        objectGraph.inject(this);
    }

    @Test
    public void givenAPresenter_WhenStart_ThenShouldShowPhoto() {
        Photo photo = new Photo();
        presenter.setView(mockView);
        presenter.start(photo);
        verify(mockView).showPhoto(eq(photo));
    }

    @Test
    public void givenAPresenter_WhenStart_ThenShouldShowProgressbar() {
        Photo photo = new Photo();
        presenter.setView(mockView);
        presenter.start(photo);
        verify(mockView).showProgressBar();
    }

    @Test
    public void givenAPresenter_WhenPhotoIsLoaded_ThenShouldHideProgressbar() {
        presenter.setView(mockView);
        presenter.onPhotoLoaded();
        verify(mockView).hideProgressBar();
    }

    @Test
    public void givenAPresenter_WhenPhotoIsLoaded_ThenShouldShowZoomTutorial() {
        presenter.setView(mockView);
        presenter.onPhotoLoaded();
        verify(mockView).showZoomTutorial();
    }

    @Test
    public void givenAPresenter_WhenPhotoLoadedError_ThenShouldHideProgressbar() {
        presenter.setView(mockView);
        presenter.onPhotoLoadedError();
        verify(mockView).hideProgressBar();
    }

    @Test
    public void givenAPresenter_WhenPhotoLoadedError_ThenShouldShowError() {
        presenter.setView(mockView);
        presenter.onPhotoLoadedError();
        verify(mockView).showError();
    }

}
