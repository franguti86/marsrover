package gutierrez.francisco.marsrover.ui;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import gutierrez.francisco.marsrover.domain.DomainModule;
import gutierrez.francisco.marsrover.domain.interactors.RetrievePhotosInteractor;
import gutierrez.francisco.marsrover.domain.interactors.RetrievePhotosInteractorImpl;
import gutierrez.francisco.marsrover.ui.presenters.PhotoDetailPresenterTest;
import gutierrez.francisco.marsrover.ui.presenters.PhotoListPresenterTest;

import static org.mockito.Mockito.mock;

@Module(
    includes = {
        DomainModule.class
    },
    injects = {
        PhotoListPresenterTest.class, PhotoDetailPresenterTest.class
    },
    overrides = true,
    library = true
)
public class TestUIModule {

  @Provides @Singleton
  RetrievePhotosInteractor provideRetrievePhotosInteractor(
      RetrievePhotosInteractorImpl retrievePhotosInteractorImpl) {
    return mock(RetrievePhotosInteractor.class);
  }

  @Provides @Singleton
  Navigator provideNavigator() {
    return mock(Navigator.class);
  }

}
