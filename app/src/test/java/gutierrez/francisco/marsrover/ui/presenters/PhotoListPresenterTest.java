package gutierrez.francisco.marsrover.ui.presenters;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;

import javax.inject.Inject;

import dagger.ObjectGraph;
import gutierrez.francisco.marsrover.domain.interactors.RetrievePhotosInteractor;
import gutierrez.francisco.marsrover.entities.Photo;
import gutierrez.francisco.marsrover.ui.Navigator;
import gutierrez.francisco.marsrover.ui.TestUIModule;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;

public class PhotoListPresenterTest {

    @Inject
    PhotoListPresenter presenter;
    @Inject
    RetrievePhotosInteractor mockRetrievePhotosInteractor;
    @Inject
    Navigator mockNavigator;

    @Mock
    private PhotoListView mockView;

    @Before
    public void setUp() throws IOException {
        MockitoAnnotations.initMocks(this);
        ObjectGraph objectGraph = ObjectGraph.create(new TestUIModule());
        objectGraph.inject(this);
    }

    @Test
    public void givenAPresenter_WhenStart_ThenShouldRetrievePhotos() {
        presenter.setView(mockView);
        presenter.start();
        verify(mockRetrievePhotosInteractor).retrievePhotos(any(RetrievePhotosInteractor.Callback.class));
    }

    @Test
    public void givenAPresenter_WhenStart_ThenShouldShowProgressbar() {
        presenter.setView(mockView);
        presenter.start();
        verify(mockView).showProgressBar();
    }

    @Test
    public void givenAPresenter_WhenPerformShowPhotoDetail_ThenShouldCallNavigatorOpenPhotoDetail() {
        Photo photo = new Photo();
        presenter.setView(mockView);
        presenter.performShowPhotoDetail(photo);
        verify(mockNavigator).openPhotoDetail(eq(photo));
    }

    @Test
    public void givenAPresenter_WhenRetrievePhotosSuccess_ThenShouldHideProgressbar() {
        Photo[] photos = new Photo[0];
        presenter.setView(mockView);
        presenter.onRetrievePhotosSuccess(photos);
        verify(mockView).hideProgressBar();
    }

    @Test
    public void givenAPresenter_WhenRetrievePhotosSuccess_ThenShouldShowPhotos() {
        Photo[] photos = new Photo[0];
        presenter.setView(mockView);
        presenter.onRetrievePhotosSuccess(photos);
        verify(mockView).showPhotos(eq(photos));
    }

    @Test
    public void givenAPresenter_WhenRetrievePhotosError_ThenShouldHideProgressbar() {
        presenter.setView(mockView);
        presenter.onRetrievePhotosError();
        verify(mockView).hideProgressBar();
    }

    @Test
    public void givenAPresenter_WhenRetrievePhotosError_ThenShouldShowError() {
        presenter.setView(mockView);
        presenter.onRetrievePhotosError();
        verify(mockView).showError();
    }

}
