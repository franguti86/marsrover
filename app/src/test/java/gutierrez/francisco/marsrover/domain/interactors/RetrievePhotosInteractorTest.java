package gutierrez.francisco.marsrover.domain.interactors;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import javax.inject.Inject;

import dagger.ObjectGraph;
import gutierrez.francisco.marsrover.FileUtils;
import gutierrez.francisco.marsrover.domain.TestDomainModule;
import gutierrez.francisco.marsrover.entities.Photo;
import okhttp3.HttpUrl;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;

import static junit.framework.Assert.fail;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

public class RetrievePhotosInteractorTest {

    @Inject
    RetrievePhotosInteractor retrievePhotosInteractor;

    private MockWebServer mockWebServer;

    @Before
    public void setUp() throws IOException {
        mockWebServer = new MockWebServer();
        mockWebServer.start();
        HttpUrl serverUrl = mockWebServer.url("");

        ObjectGraph objectGraph = ObjectGraph.create(new TestDomainModule(serverUrl.toString()));
        objectGraph.inject(this);
    }

    @Test
    public void testConfigure() throws IOException {
        File file = FileUtils.getFileFromPath(this, "./photos.json");
        String searchJson = FileUtils.readFileAsString(file);
        assertThat("Assertion error reading file!", searchJson != null && searchJson.length() > 0);
    }

    @Test public void shouldReturnPhotos() throws IOException, InterruptedException {
        File file = FileUtils.getFileFromPath(this, "./photos.json");
        String searchJson = FileUtils.readFileAsString(file);
        mockWebServer.enqueue(new MockResponse().setBody(searchJson));

        retrievePhotosInteractor.retrievePhotos(new RetrievePhotosInteractor.Callback() {
            @Override
            public void onRetrievePhotosSuccess(Photo[] photos) {
                assertThat("Assertion fail, no photos result!", photos.length > 0);
            }

            @Override
            public void onRetrievePhotosError() {
                fail("Fail retrieving photos!");
            }
        });
    }

    @Test public void shouldReturnError() throws IOException {
        mockWebServer.enqueue(new MockResponse().setBody("[,"));

        RetrievePhotosInteractor.Callback mockCallback = mock(RetrievePhotosInteractor.Callback.class);
        retrievePhotosInteractor.retrievePhotos(mockCallback);
        verify(mockCallback).onRetrievePhotosError();
        verifyNoMoreInteractions(mockCallback);
    }

    @After
    public void tearDown() throws IOException {
        mockWebServer.shutdown();
    }

}
