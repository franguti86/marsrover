package gutierrez.francisco.marsrover.domain;

import com.squareup.okhttp.Dispatcher;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.AbstractExecutorService;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import gutierrez.francisco.marsrover.di.Named;
import gutierrez.francisco.marsrover.domain.interactors.RetrievePhotosInteractorTest;
import gutierrez.francisco.marsrover.utils.CurlLoggingInterceptor;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

@Module(
        includes = {
                DomainModule.class
        },
        injects = {
                RetrievePhotosInteractorTest.class
        },
        overrides = true)
public class TestDomainModule {

    private String nasaEndpoint;

    public TestDomainModule(String nasaEndpoint) {
        this.nasaEndpoint = nasaEndpoint;
    }

    @Provides
    @Named("nasa_endpoint")
    String provideNasaEndpoint() {
        return nasaEndpoint;
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient() {
        OkHttpClient okClient = new OkHttpClient();
        okClient.setDispatcher(new Dispatcher(new SameThreadExecutorService()));
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        okClient.interceptors().add(httpLoggingInterceptor);
        okClient.interceptors().add(new CurlLoggingInterceptor());
        return okClient;
    }

    @Provides
    Retrofit provideRestAdapter(OkHttpClient okClient, @Named("nasa_endpoint") String endpoint) {
        return new Retrofit.Builder()
                .baseUrl(endpoint)
                .client(okClient)
                .callbackExecutor(new SynchronousExecutor())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    static final class SynchronousExecutor implements Executor {
        @Override public void execute(Runnable r) {
            r.run();
        }
    }

    static final class SameThreadExecutorService extends AbstractExecutorService {

        private volatile boolean terminated;

        @Override
        public void shutdown() {
            terminated = true;
        }

        @Override
        public boolean isShutdown() {
            return terminated;
        }

        @Override
        public boolean isTerminated() {
            return terminated;
        }

        @Override
        public boolean awaitTermination(long theTimeout, TimeUnit theUnit) throws InterruptedException {
            shutdown();
            return terminated;
        }

        @Override
        public List<Runnable> shutdownNow() {
            return Collections.emptyList();
        }

        @Override
        public void execute(Runnable theCommand) {
            theCommand.run();
        }
    }
}
