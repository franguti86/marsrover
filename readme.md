# Mars Rover

## Estimation

- **Analysis:** 1 hour.
- **Structure, api and domain layer:** 1 hour.
- **Photo list screen:** 2 hours.
- **photo detail screen:** 2 hours.
- **Polish app:** 1 hour.

## Design decision

I decided to apply [The Clean Architecture] from Robert Martin (Uncle Bob) using MVP for the UI layer. The reason to use this kind of architecture is to have an architecture independent to the UI, independent of the framework, and very important, testable. The domain and model layer can be easily changed, they are independent to the framework. To improve the architecture and to make easier to test the app, I used dependency injection (Dagger from Square Open Source) in every single layer. To fetch the data from the network and perform it on background I used Retrofit.

[The Clean Architecture]:http://blog.8thlight.com/uncle-bob/2012/08/13/the-clean-architecture.html